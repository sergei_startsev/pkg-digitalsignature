﻿//**Authentificate modal window state**
GlobalApp.Models.AuthModalWindowState = Backbone.Model.extend({
    //Default values.
    defaults: function () {
        return {
            certList: [],
            currentCertID: 0,
            cryptoAPI: "",
            loginErrorMessage: ""
        };
    },

    //Overwrite default Backbone.sync for this model
    sync: function (method, model, options) {
        var self = this;
        //set default values
        this.set("certList", this.defaults().certList);
        this.set("currentCertID", this.defaults().currentCertID);

        this.trigger("request");

        this.get("cryptoAPI").getCerts(
            function (response) {
                options.success(response);
            },
            function (response) {
                options.error(response);
            }
        );
    }
});

GlobalApp.Models.Instances.authModalWindowState = new GlobalApp.Models.AuthModalWindowState({ cryptoAPI: new CryptoAPI() });

//**AuthModalWindowView**
GlobalApp.Views.AuthModalWindowView = Backbone.View.extend({
    //DOM element.
    el: $("#authModalWindow"),

    events: {
        "click .no-data-refresh": "refresh",
        "click .login": "onLogin",
        "keypress #pwd": "enterPressed"
    },

    //Initialize model.
    initialize: function () {
        // Subscribe on `sync` event.
        this.model.on('sync', this.sync, this);
        // Subscribe on `request` event.
        this.model.on('request', this.request, this);
        // Subscribe on `error` event.
        this.model.on('error', this.error, this);
        // Subscribe on `error` event.
        Backbone.on("authModalWindowView:show", this.show, this);
        Backbone.on("authModalWindowView:loginSuccess", this.loginSuccess, this);
        Backbone.on("authModalWindowView:loginFail", this.loginFail, this);
        Backbone.on("authModalWindowView:xhrError", this.error, this);
    },

    refresh: function () {
        var viewModel = this.model;
        viewModel.fetch({
            success: function (model, resp, options) {
                viewModel.set("certList", $.parseJSON(resp).CertificateList);
                //get default certificate in the cert list
                viewModel.set("currentCertID", viewModel.get("certList")[viewModel.defaults().currentCertID].CertificateID);
            },
            error: function (model, resp, options) {
                //paste your code here if require
            }
        });
    },

    //View templates.
    templates: function (name) {
        var _templates = {
            "loading": "Scripts/templates/loading.html",
            "auth-modal-window": "Scripts/templates/general/auth-modal-window.html"
        };
        return GlobalApp.path + _templates[name];
    },

    //Show modal window
    show: function () {
        this.render();
    },

    render: function () {
        Logger.log("Render AuthModalWindowView");

        var kendoWindow = this.$el.data("kendoWindow");
        
        if (typeof kendoWindow === "undefined") {
            kendoWindow = this.$el.kendoWindow({
                width: "500px",
                resizable: false,
                title: "Авторизуйтесь, пожалуйста",
                modal: true,
                activate: function () {
                    $("#pwd").focus();
                }
            }).data("kendoWindow");

            kendoWindow.center();
            kendoWindow.open();

            this.refresh();
        }

        if (typeof kendoWindow !== "undefined") {
            if (kendoWindow._closing) {
                kendoWindow.center();
                kendoWindow.open();

                this.refresh();
            }
        }

        return this;
    },

    onClose: function(){
        var kendoWindow = this.$el.data("kendoWindow");
        kendoWindow.close();
    },

    onLogin: function () {
        var pwd = $("#pwd", this.$el).val();
        var kendoWindow = this.$el.data("kendoWindow");
        
        var certNum = $(".cert-list option:selected", this.$el).val();
        var certList = this.model.get("certList");
        this.model.set("currentCertID", certList[certNum].CertificateID);
        //clear error mesaage
        this.model.set("loginErrorMessage", this.model.defaults().loginErrorMessage);

        Backbone.trigger("authModalWindowView:login", { "certID": this.model.get("currentCertID"), "password": pwd });

        this.request();
    },

    enterPressed: function (event) {
        if (event.which == 13 || event.keyCode == 13) {
            this.onLogin();
            return false;
        }
    },

    loginSuccess: function () {
        this.onClose();
    },

    loginFail: function (errorMessage) {
        if (typeof errorMessage !== "undefined"){
            this.model.set("loginErrorMessage", errorMessage);
        }
        this.refresh();
    },

    request: function () {
        Logger.log("Request authModalWindowView");

        // create DOM element for loader
        var kendoWindow = this.$el.data("kendoWindow");
        kendoWindow.content("<div class='loading-wrapper'></div>");
        $(".loading-wrapper", this.$el).after("<div class='error-message-block'></div>");
        //Add ajax loader.
        $(".loading-wrapper", this.$el).html(TemplateManager.get(this.templates("loading"))());
        //Empty `error-message-block` block
        $(".error-message-block", this.$el).empty();
    },

    sync: function () {
        Logger.log("Sync authModalWindowView");
        //Remove ajax loader.
        $(".loading-wrapper", this.$el).remove();
        //Empty `error-message-block` block
        $(".error-message-block", this.$el).empty();

        var kendoWindow = this.$el.data("kendoWindow");
        kendoWindow.content(TemplateManager.get(this.templates("auth-modal-window"))());

        var certList = this.model.get("certList");

        var select = $('.cert-list', this.$el);
        var options = select.prop('options');
        $.each(certList, function (val, text) {
            options[options.length] = new Option(text.CertificateName, val);
        });

        var self = this;

        $(".cert-list", this.$el).kendoComboBox();
        $(".cert-list input", this.$el).attr("readonly", "readonly");

        if (this.model.get("loginErrorMessage") != this.model.defaults().loginErrorMessage) {
            $(".js-login-error-message", this.$el).html(this.model.get("loginErrorMessage"));
        }

        $("#pwd", this.$el).focus();
    },

    error: function (xhr) {
        Logger.log("Error authModalWindowView");

        $(".loading", this.$el).hide();
        ErrorProcessor.showErrorOnPage(xhr, $(".error-message-block", this.$el));
    }
});
GlobalApp.Views.Instances.authModalWindowView = new GlobalApp.Views.AuthModalWindowView({ model: GlobalApp.Models.Instances.authModalWindowState });
