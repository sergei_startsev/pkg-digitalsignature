﻿//**Certificate chooser state**
GlobalApp.Models.CertChooserState = Backbone.Model.extend({
    //Default values.
    defaults: function () {
        return {
            certList: [],
            currentCertID: 0
        };
    }
});

GlobalApp.Models.Instances.certChooserState = new GlobalApp.Models.CertChooserState();

//Subscribe this model on `updateCertChooserState` event.
GlobalApp.Models.Instances.certChooserState.on("updateCertChooserState", function () {
    var self = this;
    //set default values
    self.set("certList", self.defaults().certList);
    self.set("currentCertID", self.defaults().currentCertID);

    self.trigger("request");

    GlobalApp.Views.Instances.certChooserView.cryptoAPI.getCerts(
	    function (response) {
	        self.set("certList", $.parseJSON(response).CertificateList);
	        self.set("currentCertID", self.get("certList")[self.get("currentCertID")].CertificateID);
	        self.trigger("sync", response);
	    },
        function (response) {
           self.trigger("error", response);
        }
    );
});

//**CertChooserView**
GlobalApp.Views.CertChooserView = Backbone.View.extend({
    //DOM element.
    el: $("#certChooser"),

    cryptoAPI: "",

    targetEvent: "none",

    events: {
        "click .no-data-refresh": "refresh",
        "click .ok-select-cert": "onAccept",
        "click .close-select-cert": "onClose",
        "keypress #pwd": "enterPressed"
    },

    //Initialize model.
    initialize: function () {
        this.cryptoAPI = new CryptoAPI();
        // Subscribe on `sync` event.
        this.model.on('sync', this.sync, this);
        // Subscribe on `request` event.
        this.model.on('request', this.request, this);
        // Subscribe on `error` event.
        this.model.on('error', this.error, this);
    },

    refresh: function (event) {
        this.model.trigger("updateCertChooserState");
    },

    //View templates.
    templates: function (name) {
        var _templates = {
            "loading": "Scripts/templates/loading.html",
            "cert-chooser": "Scripts/templates/certificate-list.html"
        };
        return GlobalApp.path + _templates[name];
    },

    show: function (targetEvent) {
        /// <summary>
        /// Show certificate chooser 
        /// </summary>
        /// <param name="target">initiator name that initiated this method</param>/
        this.targetEvent = targetEvent;
        this.renderCertChooser();
    },

    renderCertChooser: function () {
        Logger.log("Render CertChooser");

        var self = this;
        var kendoWindow = $(this.el).kendoWindow({
            width: "500px",
            resizable: false,
            title: "Выберите сертификат",
            modal: true,
            activate: function () {
                $("#pwd", self.$el).focus();
            },
            close: function () {
                self.$el.trigger("close");
            }
        }).data("kendoWindow");

        kendoWindow.center();
        kendoWindow.open();
        $(this.el).show();

        this.model.trigger("updateCertChooserState");

        return this;
    },

    onClose: function(){
        var kendoWindow = $(this.el).data("kendoWindow");
        kendoWindow.close();
    },

    onAccept: function(){
        var pwd = $("#pwd", this.$el).val();
        var kendoWindow = $(this.el).data("kendoWindow");
        
        var certNum = $(".cert-list option:selected", $(this.el)).val();
        var certList = this.model.get("certList");
        this.model.set("currentCertID", certList[certNum].CertificateID);

        $(this.el).trigger("accepted", { "certID": this.model.get("currentCertID"), "password": pwd, "targetEvent": this.targetEvent });

        kendoWindow.close();
    },

    enterPressed: function (event) {
        if (event.which == 13 || event.keyCode == 13) {
            this.onAccept();
            return false;
        }
    },

    request: function () {
        Logger.log("Request certChooserView");

        // create DOM element for loader
        var kendoWindow = $(this.el).data("kendoWindow");
        kendoWindow.content("<div class='loading-wrapper'></div>");
        $(".loading-wrapper", this.$el).after("<div class='error-message-block'></div>");
        //Add ajax loader.
        $(".loading-wrapper", this.$el).html(TemplateManager.get(this.templates("loading"))());
        //Empty `error-message-block` block
        $(".error-message-block", this.$el).empty();
    },

    sync: function () {
        Logger.log("Sync certChooserView");
        //Remove ajax loader.
        $(".loading-wrapper", this.$el).remove();
        //Empty `error-message-block` block
        $(".error-message-block", this.$el).empty();

        var kendoWindow = $(this.el).data("kendoWindow");
        kendoWindow.content(TemplateManager.get(this.templates("cert-chooser"))());

        var certList = this.model.get("certList");

        var select = $('.cert-list', $(this.el));
        var options = select.prop('options');
        $.each(certList, function (val, text) {
            options[options.length] = new Option(text.CertificateName, val);
        });

        var self = this;

        $(".cert-list", $(this.el)).kendoComboBox();
        $(".cert-list input", $(this.el)).attr("readonly", "readonly");
        $("#pwd", $(this.el)).focus();
    },

    error: function (xhr) {
        Logger.log("Error certChooserView");

        $(".loading", this.$el).hide();
        ErrorProcessor.showErrorOnPage(xhr, $(".error-message-block", this.$el));
    }
});
GlobalApp.Views.Instances.certChooserView = new GlobalApp.Views.CertChooserView({ model: GlobalApp.Models.Instances.certChooserState });
